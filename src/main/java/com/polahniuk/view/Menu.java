package com.polahniuk.view;

import com.polahniuk.model.*;
import org.apache.logging.log4j.*;

import java.io.*;
import java.util.*;

/**
 * Main view of project. Show menu in console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    byte[] fileArray;

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     */
    public Menu() {
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::serializeShip);
        methods.put("2", this::deserializeShip);
        methods.put("3", this::readingPerformance);
        methods.put("4", this::writingPerformance);
        methods.put("5", this::myInputStream);
        methods.put("6", this::javaSourceCodeReader);
        methods.put("7", this::dosCommands);
        show();
    }

    /**
     * View how to serialize {@link Ship} object.
     */
    private void serializeShip() {
        Ship ship = new Ship(5);
        ship.addDroid(new Droid());
        ship.addDroid(new Droid());
        ship.addDroid(new Droid());
        ship.addDroid(new Droid());
        ship.addDroid(new Droid());

        try (ObjectOutput oos = new ObjectOutputStream(
                new FileOutputStream("ship.ser"))) {
            oos.writeObject(ship);
        } catch (FileNotFoundException e) {
            log.info(e);
        } catch (IOException e) {
            log.info(e);
        }
        System.out.println("Object " + ship + " was serialized.");
    }

    /**
     * View how to deserialize {@link Ship} object.
     */
    private void deserializeShip() {
        Ship ship = null;
        try (ObjectInput ois = new ObjectInputStream(
                new FileInputStream("ship.ser")
        )) {
            ship = (Ship) ois.readObject();

        } catch (FileNotFoundException | ClassNotFoundException e) {
            log.info(e);
        } catch (IOException e) {
            log.info(e);
        }
        if (ship == null) {
            System.out.println("Object was not deserialized.");
        } else {
            System.out.println("Object " + ship + " was deserialized.");
        }
    }

    /**
     * Compare reading performance of usual and buffered streams.
     */
    private void readingPerformance() {
        long start;
        long end;
        start = System.currentTimeMillis();
        System.out.println("Reading usual performance:");
        File file = new File("Java.pdf");
        try (FileInputStream fis = new FileInputStream(file)) {
            fileArray = new byte[(int) file.length()];
            int i = 0;
            while ((fileArray[i] = (byte) fis.read()) != -1) {
                i++;
            }
        } catch (FileNotFoundException e) {
            log.info(e);
        } catch (IOException e) {
            log.info(e);
        }
        end = System.currentTimeMillis();
        System.out.println("Performance = " + (end - start) + "millis");

        start = System.currentTimeMillis();
        System.out.println("Reading buffered performance:");
        try (DataInputStream dis = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("Java.pdf")))) {
            while (true) {
                byte b = dis.readByte();
            }
        } catch (FileNotFoundException e) {
            log.info(e);
        } catch (IOException e) {
            log.info(e);
        }
        end = System.currentTimeMillis();
        System.out.println("Performance = " + (end - start) + "millis");
    }

    /**
     * Compare writing performance of usual and buffered streams.
     */
    private void writingPerformance() {
        long start;
        long end;
        start = System.currentTimeMillis();
        System.out.println("Writing usual performance:");
        if (fileArray == null) {
            return;
        }
        File file = new File("JavaWrite.pdf");
        try (FileOutputStream fos = new FileOutputStream(file)) {
            for (int i = 0; i < fileArray.length; i++) {
                fos.write(fileArray[i]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        end = System.currentTimeMillis();
        System.out.println("Performance = " + (end - start) + "millis");

        start = System.currentTimeMillis();
        System.out.println("Writing buffered performance:");
        try (DataOutputStream dos = new DataOutputStream(
                new BufferedOutputStream(
                        new FileOutputStream("JavaWrite2.pdf")))) {
            dos.write(fileArray, 0, fileArray.length);
        } catch (FileNotFoundException e) {
            log.info(e);
        } catch (IOException e) {
            log.info(e);
        }
        end = System.currentTimeMillis();
        System.out.println("Performance = " + (end - start) + "millis");
    }

    /**
     * Implementation of {@link InputStream}. Not full working.
     */
    private void myInputStream() {
        byte[] bytes = {1, 23, 4, 5, 43, 43, 5, 43, 52, 5, 3, 53, 5, 25, 4, 35, 24, 53, 5, 43, 25, 45, 23, 4};
        try {
            InputStream mis = new MyInputStream(bytes);
            int[] newBites = new int[bytes.length];
            int i = 0;
            int read = mis.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * View reader of different comments from {@link Menu} class.
     */
    private void javaSourceCodeReader() {
        File file = new File("src/main/java/com/polahniuk/view/Menu.java");
        JavaSourceCodeReader jscr = new JavaSourceCodeReader(file);
        List<String> comments = jscr.getComments();
        for (String s : comments) {
            System.out.println(s);
        }
    }

    /**
     * Implementation of DOS commands like 'cd' and 'dir'.
     */
    private void dosCommands() {
        CdAndDirImpl cd = new CdAndDirImpl();
        String input = "";
        while (!input.toLowerCase().equals("q")) {
            System.out.println(cd.getFile().getPath());
            input = sc.nextLine();
            for (String s : cd.input(input)) {
                System.out.println(s);
            }
        }
    }

    /**
     * Creating menu.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Serialize ship");
        menu.put("2", "2 - Deserialize ship");
        menu.put("3", "3 - Reading performance");
        menu.put("4", "4- Writing performance");
        menu.put("5", "5- MyInputStream");
        menu.put("6", "6- Show comments from Menu.java");
        menu.put("7", "7- DOS commands implementation");
        menu.put("8", "8- DOS commands implementation");
        menu.put("Q", "Q - Exit");
    }

    /**
     * Show menu in console.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}
