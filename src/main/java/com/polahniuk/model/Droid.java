package com.polahniuk.model;

import java.io.Serializable;

/**
 * Simple class to show serialize and deserialize in java.
 */
public class Droid implements Serializable {

    private String name;
    private int power;
    transient private static int count = 0;

    public Droid(String name, int power) {
        this.name = name;
        this.power = power;
        count++;
    }

    public Droid() {
        name = "Droid" + count;
        power = 100;
        count++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", power=" + power +
                '}';
    }
}
