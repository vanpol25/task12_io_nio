package com.polahniuk.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class implementation of DOS commands like 'cd' and 'dir'.
 * Works with {@link File} class.
 */
public class CdAndDirImpl {

    /**
     * Main field which uses to implement commands 'cd' and 'dir'.
     */
    private File file;
    private Logger log = LogManager.getLogger(CdAndDirImpl.class);

    /**
     * Creates File object wit default directory.
     */
    public CdAndDirImpl() {
        file = new File("D:");
    }

    /**
     * Creates File object wit custom directory.
     *
     * @param path - custom directory.
     */
    public CdAndDirImpl(String path) {
        file = new File(path);
    }

    /**
     * @return - {@link File} object.
     */
    public File getFile() {
        return file;
    }

    /**
     * Method to input command.
     *
     * @param command - command.
     * @return - list of strings. e.g. to display in console.
     */
    public List<String> input(String command) {
        List<String> list = new ArrayList<>();
        if (command.startsWith("cd")) {
            list = cdCommand(command);
        } else if (command.startsWith("dir")) {
            list = dirCommand();
        } else {
            log.info("No such command");
            list.add("No such command");
        }
        return list;
    }

    /**
     * Implementation of 'cd' command.
     *
     * @param command - cd command.
     * @return - list of strings. e.g. to display in console.
     */
    private List<String> cdCommand(String command) {
        List<String> list = new ArrayList<>();
        String subCommand = command.substring(3);
        String path = file.getPath();
        if (subCommand.startsWith("..\\")) {
            path = file.getParent();
        }
        Pattern pattern = Pattern.compile("\\b\\w+?\\b");
        Matcher matcher = pattern.matcher(subCommand);
        while (matcher.find()) {
            path += "\\" + matcher.group();
        }
        File temp = new File(path);
        if (temp.exists()) {
            file = temp;
        } else {
            list.add("No such directory");
        }
        return list;
    }

    /**
     * Implementation of 'dir' command.
     *
     * @return - list of strings. e.g. to display in console.
     */
    private List<String> dirCommand() {
        List<String> list = new ArrayList<>();
        File[] files = file.listFiles();
        for (File f : files) {
            if (f.isFile()) {
                list.add("File: " + f.getName());
            }
            if (f.isDirectory()) {
                list.add("Directory: " + f.getName());
            }
        }
        return list;
    }
}
