package com.polahniuk.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link InputStream}.
 * Not working as it should.
 */
public class MyInputStream extends InputStream {

    private int index;

    private List<byte[]> inputBuffer = new ArrayList<>();

    /**
     *
     * @param bytes - bytes array.
     */
    public MyInputStream(byte[] bytes) {
        inputBuffer.add(bytes);
    }

    /**
     * Implementation of {@link InputStream#read()}
     * @return - byte as int. Or -1 if ends.
     * @throws IOException - some problem.
     */
    @Override
    public int read() throws IOException {
        if (inputBuffer.isEmpty()) {
            return -1;
        }
        byte[] bytes = inputBuffer.get(0);
        byte result = bytes[index++];
        if (index >= bytes.length) {
            inputBuffer.remove(0);
            index = 0;
        }
        return result;
    }

    /**
     *
     * @param b - byte array.
     * @param off - reads from this byte.
     * @param len - length of bytes.
     * @return - byte as int. Or -1 if ends.
     * @throws IOException - some problem.
     */
    @Override
    public int read(byte b[], int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
        if (inputBuffer.isEmpty()) {
            return -1;
        }
        int read = 0;
        do {
            byte[] bytes = inputBuffer.get(0);
            int lg = Math.min(bytes.length - index, len);
            System.arraycopy(bytes, index, b, off, lg);
            read += lg;
            off += lg;
            index += lg;
            len -= lg;
            if (index >= bytes.length) {
                inputBuffer.remove(0);
                index = 0;
            }
        } while (read < len && !inputBuffer.isEmpty());
        return read;
    }

}
