package com.polahniuk.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Reads a Java source-code file and displays all the comments. With no use regular expression.
 */
public class JavaSourceCodeReader {

    private List<String> comments = new ArrayList<>();
    private char[] sourceCode;
    private Logger log = LogManager.getLogger(JavaSourceCodeReader.class);

    /**
     * Creating array of chars from InputStream.
     *
     * @param file - {@link File}
     */
    public JavaSourceCodeReader(File file) {
        if (file.exists() && file.isFile()) {
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                sourceCode = new BufferedReader(new InputStreamReader(fileInputStream))
                        .lines().collect(Collectors.joining("\n")).toCharArray();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            createComments();
        } else {
            log.info("No such file");
        }
    }

    /**
     * Creating {@link JavaSourceCodeReader#comments}.
     */
    private void createComments() {
        if (sourceCode == null) {
            return;
        }
        for (int i = 0; i < sourceCode.length; i++) {
            if (sourceCode[i] == '/') {
                int type = commentType(i);
                if (type == 1) {
                    i = createSingle(i);
                } else if (type == 2) {
                    i = createMulti(i);
                } else if (type == 3) {
                    i = createJavaDoc(i);
                }
            }
        }
    }

    /**
     * @return - {@link JavaSourceCodeReader#comments}.
     */
    public List<String> getComments() {
        return comments;
    }

    /**
     * @param index - starts of single comment.
     * @return - index of other chars.
     */
    private int createSingle(int index) {
        StringBuilder sb = new StringBuilder("//");
        int i = index + 2;
        while (sourceCode[i] != '\n') {
            sb.append(sourceCode[i++]);
        }
        comments.add(sb.toString());
        return i;
    }

    /**
     * @param index - starts of multi comment.
     * @return - index of other chars.
     */
    private int createMulti(int index) {
        StringBuilder sb = new StringBuilder("/*");
        int i = index + 2;
        while (!(sourceCode[i - 1] == '*' && sourceCode[i] == '/')) {
            sb.append(sourceCode[i++]);
        }
        String comment = sb.append('/').toString();
        comments.add(comment);
        return i;
    }

    /**
     * @param index - starts of document comment.
     * @return - index of other chars.
     */
    private int createJavaDoc(int index) {
        StringBuilder sb = new StringBuilder("/**");
        int i = index + 3;
        while (!(sourceCode[i - 1] == '*' && sourceCode[i] == '/')) {
            sb.append(sourceCode[i++]);
        }
        String comment = sb.append('/').toString();
        comments.add(comment);
        return i;

    }

    /**
     * 1 - single line, 2 - multi line,  3 - javadoc.
     * 0 - unknown.
     *
     * @param index - index of char array {@link JavaSourceCodeReader#sourceCode}.
     * @return - type of comment.
     */
    private int commentType(int index) {
        int res;
        if (index + 3 >= sourceCode.length) {
            return 0;
        }
        if (sourceCode[index + 1] == '/') {
            res = 1;
        } else if (sourceCode[index + 1] == '*' && sourceCode[index + 2] != '*') {
            res = 2;
        } else if (sourceCode[index + 1] == '*' && sourceCode[index + 2] == '*') {
            res = 3;
        } else {
            res = 0;
        }
        return res;
    }

}
