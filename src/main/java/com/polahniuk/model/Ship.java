package com.polahniuk.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Simple class to show serialize and deserialize in java.
 */
public class Ship implements Serializable {

    private Set<Droid> ship;
    private int capacity;

    public Ship(int capacity) {
        ship = new HashSet<>(capacity);
        this.capacity = capacity;
    }

    public int size() {
        return ship.size();
    }

    public int capacity() {
        return capacity;
    }

    public Set<Droid> getShip() {
        return ship;
    }

    public void addDroid(Droid droid) {
        if (size() >= capacity()) {
            return;
        }
        ship.add(droid);
    }

    @Override
    public String toString() {
        return "Ship{" +
                "ship=" + ship +
                ", capacity=" + capacity +
                '}';
    }
}
